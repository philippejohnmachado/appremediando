import React, { Component } from "react";
import { Dimensions, AsyncStorage, View } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import MenuDrawer from "~/Components/MenuDrawer";

// Pages
import Login from "~/Pages/Login";
import Cadastro from "~/Pages/Cadastro";
import Home from "~/Pages/Home";
import NovaSenha from "~/Pages/NovaSenha";
import recuperarSenha from "~/Pages/RecuperarSenha";
import Usuario from "~/Pages/Usuario";
import DetalhesProduto from "~/Pages/DetalhesProduto";
import Orcamentos from "~/Pages/Orcamentos";
import Lojas from "~/Pages/Lojas";
import CarrinhoCompra from "~/Pages/CarrinhoCompra";
import Configuracao from "~/Pages/Configuracao";
import Receita from "~/Pages/Receita";
import Fotos from "~/Pages/Fotos";

const WIDTH = Dimensions.get("window").width;

const DrawerConfig = {
  drawerWidth: WIDTH * 0.83,
  drawerPosition: "left",
  drawerType: "slide",
  contentComponent: MenuDrawer,
  contentComponent: ({ navigation }) => {
    return <MenuDrawer navigation={navigation} />;
  }
};

const LoginNavigation = createStackNavigator({
  Login: { screen: Login, navigationOptions: { header: null } },
  Cadastro: { screen: Cadastro, navigationOptions: { header: null } },
  NovaSenha: { screen: NovaSenha, navigationOptions: { header: null } },
  recuperarSenha: {
    screen: recuperarSenha,
    navigationOptions: { header: null }
  }
});

const DrawerNavigator = createDrawerNavigator(
  {
    Home: { screen: Home, navigationOptions: { title: "Home" } },
    Usuario: { screen: Usuario, navigationOptions: { title: "Usuario" } },
    DetalhesProduto: {
      screen: DetalhesProduto,
      navigationOptions: { title: "DetalhesProduto" }
    },
    Orcamentos: {
      screen: Orcamentos,
      navigationOptions: { title: "Orçamentos" }
    },
    Lojas: { screen: Lojas, navigationOptions: { title: "Lojas" } },
    CarrinhoCompra: {
      screen: CarrinhoCompra,
      navigationOptions: { title: "Carrinho de compra" }
    },
    Configuracao: {
      screen: Configuracao,
      navigationOptions: { title: "Configuração" }
    },
    Receita: { screen: Receita, navigationOptions: { title: "Receita" } },
    Fotos: { screen: Fotos, navigationOptions: { title: "Fotos" } }
  },
  DrawerConfig
);

class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    this._loadData();
  }

  render() {
    return <View />;
  }

  _loadData = async () => {
    const data = await AsyncStorage.getItem("IsLogin");
    if (data === "1") {
      this.props.navigation.navigate("App");
    } else {
      this.props.navigation.navigate("Auth");
    }
  };
}

const Navigation2 = createStackNavigator({
  DrawerNavigator: {
    screen: DrawerNavigator,
    navigationOptions: { header: null }
  }
});

const Navigation1 = createStackNavigator({
  LoginNavigation: {
    screen: LoginNavigation,
    navigationOptions: { header: null }
  },
  DrawerNavigator: {
    screen: DrawerNavigator,
    navigationOptions: { header: null }
  }
});

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: Navigation2,
      Auth: Navigation1
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
);
