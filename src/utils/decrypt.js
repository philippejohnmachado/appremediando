import CryptoJS from "react-native-crypto-js";
import { Alert } from "react-native";

decrypt = senha => {
  const key = "P9WqTIouyxgwrJEeLG0Bj5bklHB1h6uy6HeQ";
  const bytes = CryptoJS.AES.decrypt(senha, key);
  const originalText = bytes.toString(CryptoJS.enc.Utf8);

  Alert.alert("Decrypt: " + originalText);

  return originalText;
};
export default decrypt;
