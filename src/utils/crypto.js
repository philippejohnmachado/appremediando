import CryptoJS from "react-native-crypto-js";

crypto = senha => {
  const key = "P9WqTIouyxgwrJEeLG0Bj5bklHB1h6uy6HeQ";
  const ciphertext = CryptoJS.AES.encrypt(senha, key).toString();

  return ciphertext;
};
export default crypto;
