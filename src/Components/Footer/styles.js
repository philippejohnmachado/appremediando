import { StyleSheet } from "react-native";
import colors from "~/styles/colors";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    height: 60,
    width: wp("100%"),
    backgroundColor: colors.white,
    flexDirection: "row",
    justifyContent: "space-between",
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 9 },
    shadowOpacity: 0.48,
    shadowRadius: 11.95,
    elevation: 18
  },
  buttonHome: {
    marginLeft: 10,
    marginTop: 5,
    height: 50,
    width: 50,
    backgroundColor: colors.primary,
    alignItems: "center",
    borderRadius: 50
  },
  logoHome: {
    alignSelf: "center",
    marginTop: 9
  },
  buttonStore: {
    marginLeft: 10,
    marginTop: 5,
    height: 50,
    width: 50,
    backgroundColor: colors.white,
    alignItems: "center",
    borderRadius: 50
  },
  logoStore: {
    alignSelf: "center",
    marginTop: 9
  },
  buttonCamera: {
    marginLeft: 10,
    marginTop: 5,
    height: 50,
    width: 50,
    backgroundColor: colors.white,
    alignItems: "center",
    borderRadius: 50
  },
  logoCamera: {
    alignSelf: "center",
    marginTop: 9
  },
  buttonSearch: {
    marginLeft: 10,
    marginTop: 5,
    height: 50,
    width: 50,
    backgroundColor: colors.white,
    alignItems: "center",
    borderRadius: 50
  },
  logoSearch: {
    alignSelf: "center",
    marginTop: 9
  }
});

export default styles;
