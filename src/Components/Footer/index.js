import React, { Component } from "react";
import { View, Text, TouchableOpacity, Alert } from "react-native";
import IonIcons from "react-native-vector-icons/FontAwesome";
import IonIcons2 from "react-native-vector-icons/FontAwesome5";
import colors from "~/styles/colors";
import styles from "./styles";

export default class Footer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.buttonHome} onPress={() => {}}>
          <IonIcons
            name="home"
            color={colors.white}
            size={28}
            style={styles.logoHome}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonStore} onPress={() => {}}>
          <IonIcons2
            name="store"
            color={colors.primary}
            size={20}
            style={styles.logoStore}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonCamera} onPress={() => {}}>
          <IonIcons
            name="camera"
            color={colors.primary}
            size={20}
            style={styles.logoCamera}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonSearch} onPress={() => {}}>
          <IonIcons
            name="search"
            color={colors.primary}
            size={20}
            style={styles.logoSearch}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
