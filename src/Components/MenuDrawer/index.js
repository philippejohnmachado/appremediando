import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity
} from "react-native";
import styles from "./styles";
import IonIcons from "react-native-vector-icons/FontAwesome";
import IonIcons2 from "react-native-vector-icons/FontAwesome5";

export default class MenuDrawer extends Component {
  navLink(nav, Icon, text, pro) {
    return (
      <TouchableOpacity
        style={{ height: 50 }}
        onPress={() => this.props.navigation.navigate(nav, { Teste: pro })}
      >
        <View style={styles.linemenu}>
          <IonIcons2 name={Icon} color="#fff" size={20} style={styles.icon} />
          <Text style={styles.link}>{text}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scroller}>
          <View style={styles.topLinks}>
            <View
              style={{
                alignSelf: "center",
                marginTop: 35,
                flexDirection: "row"
              }}
            >
              <Image
                style={{}}
                resizeMode="contain"
                source={require("~/assets/iconeApp.png")}
              />
              <Text
                style={{
                  color: "#00BFFF",
                  fontSize: 30,
                  marginLeft: 5,
                  marginTop: 5
                }}
              >
                Remediando
              </Text>
            </View>
          </View>
          <View style={{ height: 110, backgroundColor: "#00BFFF" }}>
            <Text style={{ color: "#FFF", fontSize: 20, marginLeft: 10 }}>
              Enviar Receita
            </Text>
            <Text style={{ color: "#FFF", fontSize: 12, marginLeft: 10 }}>
              Envie Receita para obter orçamento:
            </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Receita")}
              style={{
                backgroundColor: "#FFF",
                width: 130,
                height: 30,
                marginLeft: 10,
                marginTop: 20,
                alignContent: "center"
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <IonIcons
                  name="plus-circle"
                  color="#00BFFF"
                  size={20}
                  style={{ marginTop: 5, marginLeft: 5 }}
                />
                <Text
                  style={{
                    color: "#00BFFF",
                    alignSelf: "center",
                    marginTop: 5,
                    marginLeft: 5
                  }}
                >
                  Enviar Receita
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.bottomLinks}>
            {this.navLink("Home", "home", "Home")}
            {this.navLink("Orcamentos", "paste", "Orçamento", false)}
            {this.navLink("Lojas", "store", "Lojas")}
            {this.navLink(
              "CarrinhoCompra",
              "shopping-cart",
              "Carrinho de compras"
            )}
            {this.navLink("Configuracao", "cog", "Configuração")}
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <Text style={styles.description}>MachadoIT-Remediando</Text>
          <Text style={styles.version}>v00.01</Text>
        </View>
      </View>
    );
  }
}
