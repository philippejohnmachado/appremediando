import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import IonIcons from "react-native-vector-icons/FontAwesome";
import IonIcons2 from "react-native-vector-icons/FontAwesome5";
import colors from "~/styles/colors";
import styles from "./styles";

export default class Header extends Component {
  state = {
    iconsFiltro: this.props.Icon,
    iconsOrcamento: this.props.IconOrcamento,
    IsOrcamento: true
  };

  iconsElement = () => {
    return (
      <IonIcons
        name="filter"
        color={colors.primary}
        size={28}
        style={style.filterIcon}
        onPress={() => {}}
      />
    );
  };

  iconsOrcamentos = () => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate("Orcamentos", { Teste: true })
        }
        hitSlop={{
          top: 20,
          bottom: 20,
          left: 20,
          right: 20,
        }}
        style={{
          marginTop: 5,
          position: "absolute",
          right: 0,
          height: 30,
          width: 30,
        }}
      >
        <IonIcons2
          name="paste"
          color={colors.primary}
          size={24}
          style={styles.filterIcon}
        />
        {this.state.IsOrcamento ? (
          <Text style={{ color: colors.primary, fontSize: 12 }}>20</Text>
        ) : null}
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.header}>
        <IonIcons
          name={this.props.IconHeader}
          color={colors.primary}
          size={28}
          style={styles.menuIcon}
          onPress={() => {
            if (this.props.IconHeader == "bars") {
              this.props.navigation.toggleDrawer();
            } else {
              this.props.navigation.goBack();
            }
          }}
        />
        <Text style={styles.title}>{this.props.Text}</Text>

        {this.state.iconsFiltro ? this.iconsElement() : null}
        {this.state.iconsOrcamento ? this.iconsOrcamentos() : null}
      </View>
    );
  }
}
