export default {
  Login: "Espanhol",
  firstValid: "Nombre válido",
  firstInvalid: "¡Por favor, introduzca su nombre de pila!",
  lastValid: "Apellido Valido!",
  lastInvalid: "Please enter your last name!",
  messageValid: "Correo valido",
  messageInvalid: "Correo inválido"
};
