export default {
  Login: "Português",
  firstValid: "Primeiro nome Válido!",
  firstInvalid: "Por favor entre com seu primeiro nome!",
  lastValid: "Sobrenome Válido!",
  lastInvalid: "Por favor insira seu sobrenome!",
  messageValid: "E-mail Valido!",
  messageInvalid: "E-mail Invalido!"
};
