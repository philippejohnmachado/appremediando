export default {
  Login: "Inglês",
  firstValid: "First name Valid!",
  firstInvalid: "Please enter your first name!",
  lastValid: "Last name Valid!",
  lastInvalid: "Please enter your last name!",
  messageValid: "Valid e-mail!",
  messageInvalid: "Invalid e-mail!"
};
