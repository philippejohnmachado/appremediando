import { Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");
const d = Dimensions.get("window");
const imageHeight = Math.round((d.width * 9) / 20);
const imageWidth = d.width;

export default {
  doubleBaseMargin: 20,
  baseMargin: 15,
  basePadding: 20,
  baseRadius: 5,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  smallMargin: 5,
  tabBarHeight: 54,
  navBarHeight: Platform.OS === "ios" ? 64 : 54,
  statusBarHeight: Platform.OS === "ios" ? 20 : 0,
  h: imageHeight,
  w: imageWidth
};
