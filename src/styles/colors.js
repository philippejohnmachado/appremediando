export default {
  white: "#FFF",
  lighter: "#EEE",
  light: "#DDD",
  lightgray: "#B5B5B5",
  regular: "#999",
  dark: "#666",
  darker: "#333",
  black: "#000",

  primary: "#00BFFF",
  secundary: "#FFFFFF",
  success: "#27AE60",
  danger: "#E37A7A",

  transparent: "transparent",
  darkTransparent: "rgba(0, 0, 0, 0.6)",
  whiteTransparent: "rgba(255, 255, 255, 0.3)"
};
