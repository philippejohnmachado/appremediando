import metrics from "./metrics";
import colors from "./colors";
import fonts from "./fonts";

export default {
  box: {
    backgroundColor: colors.white,
    borderRadius: metrics.baseRadius,
    padding: metrics.basePadding
  },
  container: {
    flex: 1,
    backgroundColor: colors.background
  },
  section: {
    margin: metrics.doubleBaseMargin
  },
  sectionTitle: {
    color: colors.text,
    fontWeight: "bold",
    fontSize: fonts.regular,
    alignSelf: "center",
    marginBottom: metrics.doubleBaseMargin
  }
};
