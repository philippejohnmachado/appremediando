import React from "react";
import { StatusBar } from "react-native";
import "~/config/ReactotronConfig";
console.disableYellowBox = true;

import Routes from "~/routes";

const App = () => (
  <>
    <StatusBar barStyle="light-content" backgroundColor="#00BFFF" />
    <Routes />
  </>
);

export default App;
