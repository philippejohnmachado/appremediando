import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  ActivityIndicator,
} from "react-native";
import styles from "./styles";
import api from "~/services/api";

export default class Cadastro extends Component {
  state = {
    email: "",
    emailValidation: true,
    loading: false,
    errorMessage: null,
    messageValid: null,
    messageInvalid: null,
  };

  async componentDidMount() {
    this.email = "";
  }

  validationEmail = (text, type) => {
    this.setState({
      email: text,
    });
    let pattern = new RegExp(
      /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|br|com|org|net|gov|ig|gmail|bol|uol|icloud|outlook|yahoo)\b/
    );
    if (type == "email") {
      if (pattern.test(text)) {
        this.setState({
          emailValidation: true,
          messageValid: "E-mail Valido!",
        });
      } else {
        this.setState({
          emailValidation: false,
          messageInvalid: "E-mail Invalido!",
        });
      }
    }
  };

  NovaSenha = async (email) => {
    this.setState({ loading: true });
    if (email.length > 0) {
      const response = await api.get("/user/novaSenha/" + email);
      const { message, result, error } = response.data;
      if (!error && result) {
        this.props.navigation.navigate("Login");
      }
    } else {
      this.setState({
        loading: false,
        errorMessage: "Por favor, digite um email válido!",
      });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={{ alignItems: "center" }}>
            <View style={styles.contentForgotPassword}>
              <Text style={styles.titleForgotPassword} onPress={() => {}}>
                Recuperar senha
              </Text>
              <Text style={styles.descriptionForgotPassword} onPress={() => {}}>
                A sua senha provisoria será enviada para o e-mail cadastrado,
                confirme o seu e-mail por favor
              </Text>
              <View style={{ alignItems: "center" }}>
                {!!this.state.errorMessage && (
                  <Text style={{ color: "red" }}>
                    {this.state.errorMessage}
                  </Text>
                )}
                {
                  <Text>
                    {!this.state.emailValidation ? (
                      <Text style={{ color: "red" }}>
                        {this.state.messageInvalid}
                      </Text>
                    ) : (
                      <Text style={{ color: "green" }}>
                        {this.state.messageValid}
                      </Text>
                    )}
                  </Text>
                }
              </View>
              <TextInput
                style={[
                  styles.input,
                  !this.state.emailValidation
                    ? { borderWidth: 1, borderColor: "red" }
                    : null,
                ]}
                placeholder="E-mail"
                keyboardType="email-address"
                placeholderTextColor="#000000"
                fontSize={12}
                returnKeyType="go"
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={(text) => {
                  this.setState(this.validationEmail(text, "email"));
                }}
              />
              <TouchableOpacity
                style={[
                  styles.button,
                  { alignItems: "center", justifyContent: "center" },
                ]}
                onPress={() => this.NovaSenha(this.state.email)}
              >
                {this.state.loading ? (
                  <ActivityIndicator size="small" color="#333" />
                ) : (
                  <Text style={styles.buttonText}>Finalizar</Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
