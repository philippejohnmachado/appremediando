import { StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import colors from "~/styles/colors";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  scrollview: {
    marginTop: 2,
    marginBottom: 3
  },
  contentImageProduct: {
    alignSelf: "center",
    marginTop: 50
  },
  imageProduct: {
    width: 200,
    height: 200
  },
  nameProduct: {
    alignSelf: "center",
    marginTop: 30,
    color: colors.primary
  },
  codeProduct: {
    alignSelf: "center",
    fontSize: 10,
    marginTop: 10,
    color: colors.dark
  },
  contentPrice: {
    flexDirection: "row",
    alignSelf: "center"
  },
  priceOne: {
    marginRight: 5,
    marginTop: 20,
    color: colors.lightgray,
    textDecorationLine: "line-through",
    fontWeight: "bold"
  },
  priceTwo: {
    marginLeft: 5,
    marginTop: 20,
    color: colors.primary,
    fontWeight: "bold",
    fontSize: 15
  },
  contentButonAddCart: {
    marginTop: 19,
    marginLeft: 20
  },
  buttonAddCart: {
    backgroundColor: colors.primary,
    height: 25,
    width: 100,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center"
  },
  textButtonAddCart: {
    marginLeft: 5,
    color: colors.white,
    fontSize: 15,
    fontWeight: "bold"
  },
  contentInfoStory: {
    marginTop: 30,
    backgroundColor: colors.primary,
    borderWidth: 1.5,
    height: 60,
    marginLeft: 10,
    marginRight: 10,
    borderColor: colors.primary
  },
  contentInfoStoryRow: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  textNameStory: {
    alignSelf: "center",
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    fontSize: 15,
    fontWeight: "bold",
    color: colors.white
  },
  contenRowLocation: {
    flexDirection: "row"
  },
  textLocation: {
    alignSelf: "center",
    marginTop: 5,
    marginRight: 10,
    color: colors.white,
    fontSize: 15,
    fontWeight: "bold"
  },
  contentDetailsStory: {
    flexDirection: "row",
    marginLeft: 10
  },
  textDetailsStory: {
    alignSelf: "center",
    marginTop: 5,
    marginRight: 10,
    color: colors.dark
  },
  linkDetailsStory: {
    alignSelf: "center",
    marginTop: 5,
    color: colors.primary
  },
  titleDetailsProduct: {
    alignSelf: "center",
    marginTop: 20,
    color: colors.primary
  },
  textDetailsProduct: {
    alignSelf: "center",
    textAlign: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    color: "gray"
  }
});

export default styles;
