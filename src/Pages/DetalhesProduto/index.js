import React, { Component } from "react";
import {
  View,
  Dimensions,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  TextInput
} from "react-native";
import styles from "./styles";
import colors from "~/styles/colors";
import Header from "~/Components/Header";
import SliderShow from "~/Components/SliderShow";
import IonIcons from "react-native-vector-icons/FontAwesome";
import IonIcons2 from "react-native-vector-icons/FontAwesome5";
import IonIcons3 from "react-native-vector-icons/AntDesign";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

const dimensions = Dimensions.get("window");

export default class Home extends Component {
  static navigationOptions = { header: null };
  state = {
    text: "Detalhes"
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          Text={this.state.text}
          Icon={false}
          IconHeader="angle-left"
          navigation={this.props.navigation}
        />
        <ScrollView style={styles.scrollview}>
          <View style={styles.contentImageProduct}>
            <Image
              style={styles.imageProduct}
              source={require("~/assets/medium-4606.jpg")}
            />
          </View>
          <View>
            <Text style={styles.nameProduct}>RESFENOL COM 20 CÁPSULAS</Text>
          </View>
          <View>
            <Text style={styles.codeProduct}>Codigo:12345678</Text>
          </View>
          <View style={styles.contentPrice}>
            <Text style={styles.priceOne}>R$44,83</Text>
            <Text style={styles.priceTwo}>R$14,57</Text>
            <View style={styles.contentButonAddCart}>
              <TouchableOpacity style={style.buttonAddCart}>
                <IonIcons
                  name="cart-plus"
                  color={colors.white}
                  size={20}
                  style={styles.menuIcon}
                  onPress={() => {}}
                />
                <Text style={styles.textButtonAddCart}>Adicionar</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.contentInfoStory}>
            <View style={styles.contentInfoStoryRow}>
              <Text style={styles.textNameStory}>Loja 01 </Text>
              <View style={styles.contenRowLocation}>
                <Text style={styles.textLocation}>Distancia: 1 km</Text>
                <IonIcons2
                  name="map-marker-alt"
                  color={colors.white}
                  size={20}
                  style={{ marginTop: 5, marginRight: 10 }}
                  onPress={() => {}}
                />
              </View>
            </View>
          </View>
          <View style={styles.contentDetailsStory}>
            <Text style={styles.textDetailsStory}>Detalhes da loja</Text>
            <Text style={styles.linkDetailsStory}>Mais...</Text>
          </View>
          <View>
            <Text style={styles.titleDetailsProduct}>Detalhes do produto</Text>
            <Text style={styles.textDetailsProduct}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed the
              eiusmod tempor incidid ut labore et dolore magna aliqua. Isso
              significa que, no mínimo, veniam, exigiu o exercício da nossa
              vontade, não é por isso que é excomodado por todo o mundo. velit
              esse cillum dolore eu fugi nulla pariatur. Excepteur sint occaecat
              cupidatat non proident, sunt in culp qui officia deserunt mollit
              anim id est laborum.
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}
