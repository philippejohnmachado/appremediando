import React, { Component } from "react";
import {
  View,
  Dimensions,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
} from "react-native";
import styles from "./styles";
import Header from "~/Components/Header";
import SliderShow from "~/Components/SliderShow";
import IonIcons from "react-native-vector-icons/FontAwesome";
import IonIcons2 from "react-native-vector-icons/FontAwesome5";
import IonIcons3 from "react-native-vector-icons/AntDesign";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const dimensions = Dimensions.get("window");

export default class Home extends Component {
  static navigationOptions = { header: null };
  state = {
    text: "Ofertas",
    iconFavorito: false,
  };

  Favorito = () => {
    if (this.state.iconFavorito) {
      this.setState({ iconFavorito: false });
    } else {
      this.setState({ iconFavorito: true });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{ height: 60 }}>
          <Header
            Text={this.state.text}
            Icon={true}
            IconHeader="bars"
            navigation={this.props.navigation}
          />
        </View>
        <View
          style={{
            alignSelf: "center",
            marginTop: 5,
            flexDirection: "row",
            backgroundColor: "#fff",
            height: 40,
            width: wp("95%"),
            borderWidth: 1,
            borderColor: "#00BFFF",
          }}
        >
          <TextInput
            style={{ flex: 1 }}
            placeholder="O que você está procurando?"
            underlineColorAndroid="transparent"
          />
          <IonIcons
            name="search"
            color="#dcdcdc"
            size={20}
            style={{ alignSelf: "center", marginRight: 10 }}
            onPress={() => {}}
          />
        </View>
        <ScrollView style={{ marginTop: 2, marginBottom: 3 }}>
          <View style={{}}>
            <SliderShow />
          </View>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("DetalhesProduto")}
            style={styles.conteudo}
          >
            <View style={{ marginRight: 3 }}>
              <Image
                style={{ width: 110, height: 110 }}
                source={require("~/assets/medium-4606.jpg")}
              />
            </View>
            <View style={{}}>
              <Text style={{ marginTop: 5, color: "#00BFFF", fontSize: 14 }}>
                RESFENOL COM 20 CÁPSULAS
              </Text>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "#C0C0C0",
                    fontWeight: "bold",
                    marginRight: 10,
                  }}
                >
                  Vendido por:
                </Text>
                <Text>Farmacia01</Text>
              </View>
              <View style={{ marginTop: 5, flexDirection: "row" }}>
                <Text style={{ color: "#C0C0C0" }}>De:</Text>
                <Text
                  style={{
                    color: "#C0C0C0",
                    textDecorationLine: "line-through",
                    fontWeight: "bold",
                  }}
                >
                  R$44,83
                </Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ color: "#00BFFF" }}>Por:</Text>
                <Text
                  style={{ color: "#00BFFF", fontWeight: "bold", fontSize: 15 }}
                >
                  R$14,57
                </Text>
              </View>
              <View
                style={{
                  width: wp("65%"),
                  flexDirection: "row",
                  flex: 1,
                  justifyContent: "space-between",
                }}
              >
                <View style={{}}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: "#00BFFF",
                      height: 25,
                      width: 100,
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <IonIcons
                      name="cart-plus"
                      color="#fff"
                      size={20}
                      style={styles.menuIcon}
                      onPress={() => {}}
                    />
                    <Text
                      style={{
                        marginLeft: 5,
                        color: "#fff",
                        fontSize: 15,
                        fontWeight: "bold",
                      }}
                    >
                      Adicionar
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{}}>
                  <IonIcons3
                    name={this.state.iconFavorito ? "heart" : "hearto"}
                    color="#00BFFF"
                    size={20}
                    style={styles.menuIcon}
                    onPress={() => this.Favorito()}
                  />
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("DetalhesProduto")}
            style={style.conteudo}
          >
            <View style={{ marginRight: 3 }}>
              <Image
                style={{ width: 110, height: 110 }}
                source={require("~/assets/medium-4606.jpg")}
              />
            </View>
            <View style={{}}>
              <Text style={{ marginTop: 5, color: "#00BFFF", fontSize: 14 }}>
                RESFENOL COM 20 CÁPSULAS
              </Text>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "#C0C0C0",
                    fontWeight: "bold",
                    marginRight: 10,
                  }}
                >
                  Vendido por:
                </Text>
                <Text>Farmacia01</Text>
              </View>
              <View style={{ marginTop: 5, flexDirection: "row" }}>
                <Text style={{ color: "#C0C0C0" }}>De:</Text>
                <Text
                  style={{
                    color: "#C0C0C0",
                    textDecorationLine: "line-through",
                    fontWeight: "bold",
                  }}
                >
                  R$44,83
                </Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ color: "#00BFFF" }}>Por:</Text>
                <Text
                  style={{ color: "#00BFFF", fontWeight: "bold", fontSize: 15 }}
                >
                  R$14,57
                </Text>
              </View>
              <View
                style={{
                  width: wp("65%"),
                  flexDirection: "row",
                  flex: 1,
                  justifyContent: "space-between",
                }}
              >
                <View style={{}}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: "#00BFFF",
                      height: 25,
                      width: 100,
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <IonIcons
                      name="cart-plus"
                      color="#fff"
                      size={20}
                      style={styles.menuIcon}
                      onPress={() => {}}
                    />
                    <Text
                      style={{
                        marginLeft: 5,
                        color: "#fff",
                        fontSize: 15,
                        fontWeight: "bold",
                      }}
                    >
                      Adicionar
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{}}>
                  <IonIcons3
                    name={"hearto"}
                    color="#00BFFF"
                    size={20}
                    style={styles.menuIcon}
                    onPress={() => {}}
                  />
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("DetalhesProduto")}
            style={styles.conteudo}
          >
            <View style={{ marginRight: 3 }}>
              <Image
                style={{ width: 110, height: 110 }}
                source={require("~/assets/medium-4606.jpg")}
              />
            </View>
            <View style={{}}>
              <Text style={{ marginTop: 5, color: "#00BFFF", fontSize: 14 }}>
                RESFENOL COM 20 CÁPSULAS
              </Text>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "#C0C0C0",
                    fontWeight: "bold",
                    marginRight: 10,
                  }}
                >
                  Vendido por:
                </Text>
                <Text>Farmacia01</Text>
              </View>
              <View style={{ marginTop: 5, flexDirection: "row" }}>
                <Text style={{ color: "#C0C0C0" }}>De:</Text>
                <Text
                  style={{
                    color: "#C0C0C0",
                    textDecorationLine: "line-through",
                    fontWeight: "bold",
                  }}
                >
                  R$44,83
                </Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ color: "#00BFFF" }}>Por:</Text>
                <Text
                  style={{ color: "#00BFFF", fontWeight: "bold", fontSize: 15 }}
                >
                  R$14,57
                </Text>
              </View>
              <View
                style={{
                  width: wp("65%"),
                  flexDirection: "row",
                  flex: 1,
                  justifyContent: "space-between",
                }}
              >
                <View style={{}}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: "#00BFFF",
                      height: 25,
                      width: 100,
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <IonIcons
                      name="cart-plus"
                      color="#fff"
                      size={20}
                      style={styles.menuIcon}
                      onPress={() => {}}
                    />
                    <Text
                      style={{
                        marginLeft: 5,
                        color: "#fff",
                        fontSize: 15,
                        fontWeight: "bold",
                      }}
                    >
                      Adicionar
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{}}>
                  <IonIcons3
                    name={"hearto"}
                    color="#00BFFF"
                    size={20}
                    style={styles.menuIcon}
                    onPress={() => {}}
                  />
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("DetalhesProduto")}
            style={styles.conteudo}
          >
            <View style={{ marginRight: 3 }}>
              <Image
                style={{ width: 110, height: 110 }}
                source={require("~/assets/medium-4606.jpg")}
              />
            </View>
            <View style={{}}>
              <Text style={{ marginTop: 5, color: "#00BFFF", fontSize: 14 }}>
                RESFENOL COM 20 CÁPSULAS
              </Text>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "#C0C0C0",
                    fontWeight: "bold",
                    marginRight: 10,
                  }}
                >
                  Vendido por:
                </Text>
                <Text>Farmacia01</Text>
              </View>
              <View style={{ marginTop: 5, flexDirection: "row" }}>
                <Text style={{ color: "#C0C0C0" }}>De:</Text>
                <Text
                  style={{
                    color: "#C0C0C0",
                    textDecorationLine: "line-through",
                    fontWeight: "bold",
                  }}
                >
                  R$44,83
                </Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ color: "#00BFFF" }}>Por:</Text>
                <Text
                  style={{ color: "#00BFFF", fontWeight: "bold", fontSize: 15 }}
                >
                  R$14,57
                </Text>
              </View>
              <View
                style={{
                  width: wp("65%"),
                  flexDirection: "row",
                  flex: 1,
                  justifyContent: "space-between",
                }}
              >
                <View style={{}}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: "#00BFFF",
                      height: 25,
                      width: 100,
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <IonIcons
                      name="cart-plus"
                      color="#fff"
                      size={20}
                      style={styles.menuIcon}
                      onPress={() => {}}
                    />
                    <Text
                      style={{
                        marginLeft: 5,
                        color: "#fff",
                        fontSize: 15,
                        fontWeight: "bold",
                      }}
                    >
                      Adicionar
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{}}>
                  <IonIcons3
                    name={"hearto"}
                    color="#00BFFF"
                    size={20}
                    style={styles.menuIcon}
                    onPress={() => {}}
                  />
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </ScrollView>
        <View
          style={{
            height: 60,
            width: wp("100%"),
            backgroundColor: "#fff",
            flexDirection: "row",
            justifyContent: "space-between",
            shadowColor: "#000000",
            shadowOffset: { width: 0, height: 9 },
            shadowOpacity: 0.48,
            shadowRadius: 11.95,
            elevation: 18,
          }}
        >
          <TouchableOpacity
            style={{
              marginLeft: 10,
              marginTop: 5,
              height: 50,
              width: 50,
              backgroundColor: "#00BFFF",
              alignItems: "center",
              borderRadius: 50,
            }}
            onPress={() => {}}
          >
            <IonIcons
              name="home"
              color="#fff"
              size={28}
              style={{ alignSelf: "center", marginTop: 9 }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginLeft: 10,
              marginTop: 5,
              height: 50,
              width: 50,
              backgroundColor: "#fff",
              alignItems: "center",
              borderRadius: 50,
            }}
            onPress={() => {}}
          >
            <IonIcons2
              name="store"
              color="#00BFFF"
              size={20}
              style={{ alignSelf: "center", marginTop: 9 }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginLeft: 10,
              marginTop: 5,
              height: 50,
              width: 50,
              backgroundColor: "#fff",
              alignItems: "center",
              borderRadius: 50,
            }}
            onPress={() => {}}
          >
            <IonIcons
              name="camera"
              color="#00BFFF"
              size={20}
              style={{ alignSelf: "center", marginTop: 9 }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              marginLeft: 10,
              marginTop: 5,
              height: 50,
              width: 50,
              backgroundColor: "#fff",
              alignItems: "center",
              borderRadius: 50,
            }}
            onPress={() => {}}
          >
            <IonIcons
              name="search"
              color="#00BFFF"
              size={20}
              style={{ alignSelf: "center", marginTop: 9 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
