import { StyleSheet } from "react-native";
import colors from "~/styles/colors";

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: colors.white
  },
  button: {
    backgroundColor: colors.primary,
    borderColor: colors.primary,
    borderWidth: 1,
    height: 60,
    width: 60
  },
  buttonTakePicture: {
    flex: 0,
    alignSelf: "center",
    position: "absolute",
    bottom: 20
  },
  buttonCloseCamera: {
    flex: 0,
    position: "absolute",
    top: 20,
    right: 20
  }
});

export default style;
