import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage,
} from "react-native";
import styles from "./styles";
import api from "~/services/api";
// import Crypto from '../../Crypto';
import { crypto as Crypto } from "~/utils/crypto";
export default class Cadastro extends Component {
  state = {
    senha: "",
    ConfirmarSenha: "",
  };

  async componentDidMount() {
    this.senha = "";
    this.ConfirmarSenha = "";
  }

  handlePassword = (text) => {
    this.setState({ senha: text });
  };

  handleNewPassword = (text) => {
    this.setState({ ConfirmarSenha: text });
  };

  NovaSenha = async (senha, NovaSenha) => {
    const data = await AsyncStorage.getItem("DATA_USER");
    const obj = JSON.parse(data);
    const { UserId, token, message, SenhaProv, result, error } = obj;

    if (senha === NovaSenha) {
      var resultCrypto = await Crypto(senha);

      const response = await api.post("/user/novaSenha/", {
        UserId: UserId.toString(),
        token: token,
        senha: resultCrypto,
      });
      const { message, result } = response.data;

      if (message === "Senha Atualizada com sucesso" && result) {
        this.props.navigation.navigate("Home");
      }
    } else {
      Alert.alert("senhas nao confere");
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={{ alignItems: "center" }}>
            <View style={styles.contentNewPassword}>
              <Text style={styles.titleNewPassword} onPress={() => {}}>
                Registrar nova senha
              </Text>
              <TextInput
                style={styles.input}
                placeholder="*Senha"
                fontSize={12}
                secureTextEntry={true}
                returnKeyType="next"
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={this.handleNewPassword}
                value={this.state.text}
                ref={(senha) => (this.senhaInput = senha)}
                placeholderTextColor="#000000"
              />
              <TextInput
                style={styles.input}
                placeholder="*Confirmar senha"
                fontSize={12}
                secureTextEntry={true}
                returnKeyType="go"
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={this.handlePassword}
                value={this.state.text}
                ref={(senha) => (this.senhaInput = senha)}
                placeholderTextColor="#000000"
              />
              <View style={{ marginTop: 5, marginBottom: 5 }}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() =>
                    this.NovaSenha(this.state.senha, this.state.ConfirmarSenha)
                  }
                >
                  <Text style={style.textButton}>Finalizar</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
