import { StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import colors from "~/styles/colors";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  content: {
    width: wp("100%"),
    height: hp("100%"),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.primary
  },
  contentCadastro: {
    backgroundColor: colors.white,
    height: hp("90%"),
    width: wp("90%"),
    marginTop: 20,
    justifyContent: "center",
    borderRadius: 15,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 1.0,
    shadowRadius: 5
  },
  TitleRegistrar: {
    textAlign: "center",
    fontSize: 20,
    color: colors.black,
    fontWeight: "bold",
    marginTop: 10
  },
  inputRegistrar: {
    marginHorizontal: 20,
    borderWidth: 1,
    height: 35,
    paddingHorizontal: 10,
    marginTop: 20,
    borderColor: colors.primary,
    backgroundColor: colors.white
  },
  text: {
    fontSize: 30,
    color: colors.black
  },
  button: {
    backgroundColor: colors.primary,
    borderColor: colors.primary,
    borderWidth: 1,
    height: 40,
    marginHorizontal: 30,
    marginTop: 30
  },
  buttonText: {
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
    margin: 5,
    color: colors.white,
    backgroundColor: "transparent"
  }
});

export default styles;
