import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from "react-native";
import styles from "./styles";
// import Crypto from "../../Crypto";
import { crypto as Crypto } from "~/utils/crypto";
import api from "~/services/api";
// import IonIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { TextInputMask } from "react-native-masked-text";
import I18n from "~/locales/i18n";

export default class Cadastro extends Component {
  state = {
    nome: "",
    sobreNome: "",
    cpf: "",
    email: "",
    senha: "",
    confirmarSenha: "",
    secureTextEntry: true,
    // iconName: "eye-off",
    firstValidation: true,
    firstValid: null,
    firstInvalid: null,
    lastValidation: true,
    lastValid: null,
    lastInvalid: null,
    emailValidation: true,
    messageValid: null,
    messageInvalid: null
  };

  async componentDidMount() {
    this.nome = "";
    this.sobreNome = "";
    this.cpf = "";
    this.email = "";
    this.senha = "";
    this.confirmarSenha = "";
  }

  handleNome = text => {
    this.setState({ nome: text });
    if (text.length > 0 && text.match(/^[a-zA-Z ]*$/)) {
      this.setState({
        firstValidation: true,
        firstValid: <Text>*{I18n.t("firstValid")}</Text>
      });
    } else {
      this.setState({
        firstValidation: false,
        firstInvalid: <Text>*{I18n.t("firstInvalid")}</Text>
      });
    }
  };

  handleSobrenome = text => {
    this.setState({ sobreNome: text });
    if (text.length > 0 && text.match(/^[a-zA-Z ]*$/)) {
      this.setState({
        lastValidation: true,
        lastValid: <Text>*{I18n.t("lastValid")}</Text>
      });
    } else {
      this.setState({
        lastValidation: false,
        lastInvalid: <Text>*{I18n.t("lastInvalid")}</Text>
      });
    }
  };

  // handleCpf = text => {
  //   this.setState({ cpf: text });
  // };

  handleEmail = (text, type) => {
    this.setState({ email: text });
    let pattern = new RegExp(
      /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|br|com|org|net|gov|ig|gmail|bol|uol|icloud|outlook|yahoo)\b/
    );
    if (type == "email") {
      if (pattern.test(text)) {
        this.setState({
          emailValidation: true,
          messageValid: <Text>*{I18n.t("messageValid")}</Text>
        });
      } else {
        this.setState({
          emailValidation: false,
          messageInvalid: <Text>*{I18n.t("messageInvalid")}</Text>
        });
      }
    }
  };

  handleSenha = text => {
    this.setState({ senha: text });
  };

  handleConfirmarSenha = text => {
    this.setState({ confirmarSenha: text });
  };

  // onIconPress = () => {
  //   let iconName = this.state.secureTextEntry ? "eye" : "eye-off";
  //   this.setState({
  //     secureTextEntry: !this.state.secureTextEntry,
  //     iconName: iconName
  //   });
  // };

  Cadastro = async (nome, sobreNome, cpf, email, senha, confirmarSenha) => {
    try {
      if (senha === confirmarSenha) {
        var resultCrypto = await Crypto(senha);

        const response = await api.post("/user", {
          nome: nome,
          sobrenome: sobreNome,
          cpf: cpf,
          email: email,
          senha: resultCrypto
        });

        const { result, error, message } = response.data;
        if (result && !error) {
          await AsyncStorage.setItem("IsLogin", "1");
          this.props.navigation.navigate("Home");
        } else {
          Alert.alert("Ops!", message);
        }
      } else {
        Alert.alert("Ops!", "Senha nao confere");
      }
    } catch (e) {
      Alert.alert("Ops!", "error: " + e);
    }
  };

  render() {
    // const { senha } = this.state
    // const passwordValid = (senha && senha.length >= 8 ? true : false)
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={{ alignItems: "center" }}>
            <View style={styles.contentCadastro}>
              <Text style={styles.TitleRegistrar} onPress={() => {}}>
                Registar
              </Text>
              <TextInput
                style={styles.inputRegistrar}
                placeholder="*Nome"
                fontSize={12}
                returnKeyType="next"
                autoCapitalize="none"
                autoCorrect={false}
                onSubmitEditing={() => this.sobrenomeInput.focus()}
                onChangeText={this.handleNome}
                placeholderTextColor="#000000"
              />
              {
                <Text style={{ marginLeft: 20, marginBottom: -17 }}>
                  {!this.state.firstValidation ? (
                    <Text style={{ color: "red" }}>
                      {this.state.firstInvalid}
                    </Text>
                  ) : (
                    <Text style={{ color: "green" }}>
                      {this.state.firstValid}
                    </Text>
                  )}
                </Text>
              }
              <TextInput
                style={styles.inputRegistrar}
                placeholder="*Sobrenome"
                fontSize={12}
                returnKeyType="next"
                autoCapitalize="none"
                autoCorrect={false}
                onSubmitEditing={() => this.cpfInput.focus()}
                onChangeText={this.handleSobrenome}
                placeholderTextColor="#000000"
              />
              {
                <Text style={{ marginLeft: 20, marginBottom: -17 }}>
                  {!this.state.lastValidation ? (
                    <Text style={{ color: "red" }}>
                      {this.state.lastInvalid}
                    </Text>
                  ) : (
                    <Text style={{ color: "green" }}>
                      {this.state.lastValid}
                    </Text>
                  )}
                </Text>
              }
              <TextInputMask
                type={"cpf"}
                style={styles.inputRegistrar}
                placeholder="*CPF"
                returnKeyType="next"
                autoCapitalize="none"
                autoCorrect={false}
                onSubmitEditing={() => this.emailInput.focus()}
                value={this.state.cpf}
                onChangeText={text => {
                  this.setState({
                    cpf: text
                  });
                }}
                placeholderTextColor="#000000"
              />
              <TextInput
                style={styles.inputRegistrar}
                placeholder="*E-mail"
                fontSize={12}
                returnKeyType="next"
                autoCapitalize="none"
                autoCorrect={false}
                onSubmitEditing={() => this.senhaInput.focus()}
                onChangeText={text => {
                  this.setState(this.handleEmail(text, "email"));
                }}
                placeholderTextColor="#000000"
              />
              {
                <Text style={{ marginLeft: 20, marginBottom: -17 }}>
                  {!this.state.emailValidation ? (
                    <Text style={{ color: "red" }}>
                      {this.state.messageInvalid}
                    </Text>
                  ) : (
                    <Text style={{ color: "green" }}>
                      {this.state.messageValid}
                    </Text>
                  )}
                </Text>
              }
              <TextInput
                style={styles.inputRegistrar}
                placeholder="*Senha"
                secureTextEntry={true}
                // secureTextEntry={this.state.secureTextEntry}
                fontSize={12}
                returnKeyType="go"
                autoCapitalize="none"
                autoCorrect={false}
                onSubmitEditing={() => this.confirmarSenhaInput.focus()}
                onChangeText={this.handleSenha}
                placeholderTextColor="#000000"
              />
              <TextInput
                style={styles.inputRegistrar}
                placeholder="*Confirmar senha"
                secureTextEntry={true}
                // secureTextEntry={this.state.secureTextEntry}
                fontSize={12}
                returnKeyType="go"
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={this.handleConfirmarSenha}
                placeholderTextColor="#000000"
              />
              {/* <TouchableOpacity
                style={{
                  position: "absolute",
                  marginTop: -10,
                  alignSelf: "flex-end",
                  padding: 25
                }}
                onPress={this.onIconPress}
              >
                <IonIcons name={this.state.iconName} size={20} color="#111" />
              </TouchableOpacity> */}
              <View style={{ marginTop: 5, marginBottom: 5 }}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() =>
                    this.Cadastro(
                      this.state.nome,
                      this.state.sobreNome,
                      this.state.cpf,
                      this.state.email,
                      this.state.senha,
                      this.state.confirmarSenha
                    )
                  }
                >
                  <Text style={styles.buttonText}>Finalizar</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
