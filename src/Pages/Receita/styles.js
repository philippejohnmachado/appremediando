import { StyleSheet } from "react-native";
import colors from "~/styles/colors";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: colors.white,
  },
  button: {
    backgroundColor: colors.primary,
    borderColor: colors.primary,
    alignSelf: "center",
    borderWidth: 1,
    height: 50,
    width: 150,
  },
  buttonTakePicture: {
    flex: 0,
    alignSelf: "center",
    position: "absolute",
    bottom: 20,
  },
  buttonCloseCamera: {
    flex: 0,
    position: "absolute",
    top: 20,
    right: 20,
  },
  containerCamera: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: colors.white,
  },
  buttonText: {
    alignSelf: "center",
    color: colors.white,
    marginTop: 10,
  },
});

export default styles;
