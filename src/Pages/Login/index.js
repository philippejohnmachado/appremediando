import React, { Component } from "react";
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  Alert,
  KeyboardAvoidingView,
  AsyncStorage,
  TouchableHighlight,
  ActivityIndicator,
} from "react-native";
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from "react-native-fbsdk";
import IonIcons from "react-native-vector-icons/MaterialCommunityIcons";
import styles from "./styles";
import api from "~/services/api";
// import Crypto from "../../Crypto";
import { crypto as Crypto } from "~/utils/crypto";
// import I18n from "~/locales/i18n";

export default class Login extends Component {
  state = {
    email: "",
    password: "",
    user_name: "",
    token: "",
    profile_pic: "",
    Loading: false,
    secureTextEntry: true,
    iconName: "eye-off",
  };

  async componentDidMount() {
    this.email = "";
    this.senha = "";
  }

  handleEmail = (text) => {
    this.setState({ email: text });
  };

  handlePassword = (text) => {
    this.setState({ password: text });
  };

  onIconPress = () => {
    let iconName = this.state.secureTextEntry ? "eye" : "eye-off";
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
      iconName: iconName,
    });
  };

  facebook = async () => {
    LoginManager.logInWithPermissions(["email", "public_profile"]).then(
      function (result) {
        if (result.isCancelled) {
          Alert.alert("Login was cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const infoRequest = new GraphRequest(
              "/me?fields=email,name,picture",
              null,
              this._responseInfoCallback
            );
            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      },
      function (error) {
        alert("Login failed with error: " + error);
      }
    );
  };

  _responseInfoCallback = async (error, result) => {
    if (error) {
      alert("Error fetching data: " + error.toString());
    } else {
      Alert.alert("teste");
    }
  };

  Login = async (email, pass) => {
    this.setState({ Loading: true });
    var resultCrypto = await Crypto(pass);
    if (email == "" && pass == "") {
      Alert.alert("Ops!", "Digite o seu e-mail e senha.");
      this.setState({ Loading: false });
    }

    const response = await api.post("/user/validation", {
      email: email,
      senha: resultCrypto,
    });

    const { UserId, token, message, SenhaProv, result, error } = response.data;

    if (result && !SenhaProv) {
      await AsyncStorage.setItem("IsLogin", "1");
      await AsyncStorage.setItem("DATA_USER", JSON.stringify(response.data));
      this.props.navigation.navigate("Home");
    } else if (result && SenhaProv) {
      this.props.navigation.navigate("NovaSenha");
      Alert.alert("Ops!", message);
    } else if (!result && !error && message === "E-mail ou senha invalidos.") {
      this.setState({ Loading: false });
      Alert.alert("Ops!", message);
    } else {
      this.setState({ Loading: false });
      Alert.alert("Ops!", "Não foi possível processar suas informações.");
    }
  };

  render() {
    return (
      <KeyboardAvoidingView behavior="height" style={styles.container}>
        <View style={styles.content}>
          <Image
            style={styles.logo}
            resizeMode="contain"
            source={require("~/assets/Farma.png")}
          />
          <View style={{ alignItems: "center" }}>
            <Text
              style={{
                textAlign: "center",
                fontSize: 30,
                color: "#fff",
                fontWeight: "bold",
              }}
              onPress={() => {}}
            >
              REMEDIANDO
            </Text>
          </View>
          <View style={styles.contentTextInput}>
            <Text style={styles.titleLogin} onPress={() => {}}>
              Login
            </Text>
            <TextInput
              style={styles.TextInputEmail}
              placeholder="E-mail"
              keyboardType="email-address"
              placeholderTextColor="#000000"
              fontSize={12}
              returnKeyType="next"
              autoCapitalize="none"
              autoCorrect={false}
              onSubmitEditing={() => this.senhaInput.focus()}
              onChangeText={this.handleEmail}
            />
            <View>
              <TextInput
                style={styles.TextInputSenha}
                placeholder="Senha"
                secureTextEntry={this.state.secureTextEntry}
                placeholderTextColor="#000000"
                fontSize={12}
                returnKeyType="go"
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={this.handlePassword}
                value={this.state.text}
                ref={(senha) => (this.senhaInput = senha)}
              />
              <TouchableOpacity
                style={{
                  position: "absolute",
                  marginTop: -10,
                  alignSelf: "flex-end",
                  padding: 25,
                }}
                onPress={this.onIconPress}
              >
                <IonIcons name={this.state.iconName} size={20} color="#111" />
              </TouchableOpacity>
            </View>
            <Text
              style={styles.textRegistrar}
              onPress={() => this.props.navigation.navigate("Cadastro")}
            >
              Não tenho cadastro
            </Text>
            <View style={styles.contentButton}>
              <TouchableOpacity
                style={styles.button}
                onPress={() =>
                  this.Login(this.state.email, this.state.password)
                }
              >
                {this.state.Loading ? (
                  <ActivityIndicator
                    animating={this.state.Loading}
                    style={{}}
                    size="large"
                    color="#fff"
                  />
                ) : (
                  <Text style={styles.buttonText}>Entrar</Text>
                  // <Text style={{ fontWeight: "bold" }}>{I18n.t("Login")}</Text >
                )}
              </TouchableOpacity>
            </View>
            {/* <Text style={styles.textRedeSociais}>OU</Text> */}
            {/* <View style={styles.contentRedeSociais}>
       <TouchableHighlight onPress={()=>this.facebook()}>
        <Image  style={styles.imgRedeSociais}  resizeMode="contain" source={require("~/assets/facebook.png")}/>
        </TouchableHighlight>
        <Image  style={styles.imgRedeSociais}  resizeMode="contain" source={require("~/assets/google.png")}/>
        </View> */}
            <Text
              style={styles.textRecuSenha}
              onPress={() => this.props.navigation.navigate("recuperarSenha")}
            >
              Esqueci minha senha
            </Text>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
