import { StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import colors from "~/styles/colors";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: colors.primary,
  },
  content: {
    width: wp("100%"),
    height: hp("90%"),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.primary,
  },
  containerFace: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.white,
  },
  shareText: {
    fontSize: 20,
    margin: 10,
  },
  logo: {
    height: 80,
    width: 80,
    marginTop: 50,
  },
  contentTextInput: {
    flex: 2,
    marginBottom: 30,
    backgroundColor: colors.white,
    height: hp("70%"),
    width: wp("80%"),
    marginTop: 30,
    justifyContent: "center",
    borderRadius: 15,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 1.0,
    shadowRadius: 5,
  },
  titleLogin: {
    textAlign: "center",
    fontSize: 20,
    color: colors.black,
    fontWeight: "bold",
  },
  TextInputEmail: {
    marginHorizontal: 20,
    borderWidth: 1,
    height: 35,
    paddingHorizontal: 10,
    marginTop: 20,
    backgroundColor: colors.white,
  },
  TextInputSenha: {
    marginHorizontal: 20,
    borderWidth: 1,
    height: 35,
    paddingHorizontal: 10,
    marginTop: 8,
    backgroundColor: colors.white,
  },
  textRegistrar: {
    textAlign: "center",
    fontSize: 15,
    color: colors.black,
    marginTop: 8,
    textDecorationLine: "underline",
  },
  contentButton: {
    marginTop: 5,
    marginBottom: 5,
  },
  button: {
    backgroundColor: colors.primary,
    borderColor: colors.primary,
    borderWidth: 1,
    height: 40,
    marginHorizontal: 30,
    marginTop: 5,
  },
  buttonText: {
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
    margin: 5,
    color: colors.white,
    backgroundColor: "transparent",
  },
  textRedeSociais: {
    textAlign: "center",
    fontSize: 15,
    color: colors.black,
    marginBottom: 10,
  },
  contentRedeSociais: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
  imgRedeSociais: {
    height: 36,
    width: 36,
    marginRight: 10,
  },
  textRecuSenha: {
    textAlign: "center",
    fontSize: 15,
    color: colors.black,
    marginTop: 30,
    textDecorationLine: "underline",
  },
});

export default styles;
