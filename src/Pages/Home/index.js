import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView
} from "react-native";
import styles from "./styles";
import IonIcons from "react-native-vector-icons/FontAwesome";
import IonIcons2 from "react-native-vector-icons/FontAwesome5";
import IonIcons3 from "react-native-vector-icons/AntDesign";
import Header from "~/Components/Header";
import Footer from "~/Components/Footer";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

const dimensions = Dimensions.get("window");
const imageHeight = Math.round((dimensions.width * 9) / 15);
const imageWidth = dimensions.width;

export default class Home extends Component {
  static navigationOptions = { header: null };
  state = {
    text: "Home"
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{ height: 40 }}>
          <Header
            Text={this.state.text}
            Icon={false}
            IconOrcamento={true}
            IconHeader="bars"
            navigation={this.props.navigation}
          />
        </View>
        <View style={{ alignSelf: "center" }}>
          <View style={styles.ContentCard}>
            <Text
              style={{
                marginTop: 10,
                alignSelf: "center",
                textAlign: "center",
                fontSize: 18,
                color: "#FFF",
                fontWeight: "bold"
              }}
            >
              Envie suas receita para obter orçamentos
            </Text>
            <View style={{ flexDirection: "row", alignSelf: "center" }}>
              <IonIcons2
                name="clipboard-list"
                color="#FFF"
                size={30}
                style={{ marginRight: 10, marginTop: 20 }}
                onPress={() => {}}
              />
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Receita")}
                style={{
                  backgroundColor: "#FFF",
                  width: 130,
                  height: 30,
                  marginLeft: 10,
                  marginTop: 20,
                  alignContent: "center"
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <IonIcons
                    name="plus-circle"
                    color="#00BFFF"
                    size={20}
                    style={{ marginTop: 5, marginLeft: 5 }}
                  />
                  <Text
                    style={{
                      color: "#00BFFF",
                      alignSelf: "center",
                      marginTop: 5,
                      marginLeft: 5
                    }}
                  >
                    Enviar Receita
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{ height: 30, width: wp("100%") }}>
          <Text
            style={{
              marginTop: 20,
              textAlign: "center",
              color: "#00BFFF",
              fontSize: 17
            }}
          >
            Histórico de receitas
          </Text>
        </View>
        <ScrollView style={{ marginTop: 20, marginBottom: 3 }}>
          <TouchableOpacity onPress={() => {}} style={styles.conteudo}>
            <View style={{ flexDirection: "row" }}>
              <View style={{}}>
                <IonIcons2
                  name="clipboard-list"
                  color="#00BFFF"
                  size={40}
                  style={{ marginLeft: 10, marginTop: 20 }}
                  onPress={() => {}}
                />
              </View>
              <View style={{ marginLeft: 20 }}>
                <Text style={{ marginTop: 20, color: "#00BFFF", fontSize: 17 }}>
                  Data de Envio:26/02/2020{" "}
                </Text>
                <Text style={{ marginTop: 20, color: "#C0C0C0", fontSize: 12 }}>
                  Respondido
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {}}
              style={{
                alignSelf: "center",
                marginRight: 20,
                height: 20,
                width: 20
              }}
            >
              <IonIcons2
                name="chevron-right"
                color="#00BFFF"
                size={20}
                style={{}}
                onPress={() => {}}
              />
            </TouchableOpacity>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {}} style={styles.conteudo}>
            <View style={{ flexDirection: "row" }}>
              <View style={{}}>
                <IonIcons2
                  name="clipboard-list"
                  color="#00BFFF"
                  size={40}
                  style={{ marginLeft: 10, marginTop: 20 }}
                  onPress={() => {}}
                />
              </View>
              <View style={{ marginLeft: 20 }}>
                <Text style={{ marginTop: 20, color: "#00BFFF", fontSize: 17 }}>
                  Data de Envio:26/02/2020{" "}
                </Text>
                <Text style={{ marginTop: 20, color: "#C0C0C0", fontSize: 12 }}>
                  Aguardando
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {}}
              style={{
                alignSelf: "center",
                marginRight: 20,
                height: 20,
                width: 20
              }}
            >
              <IonIcons2
                name="chevron-right"
                color="#00BFFF"
                size={20}
                style={{}}
                onPress={() => {}}
              />
            </TouchableOpacity>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {}} style={styles.conteudo}>
            <View style={{ flexDirection: "row" }}>
              <View style={{}}>
                <IonIcons2
                  name="clipboard-list"
                  color="#00BFFF"
                  size={40}
                  style={{ marginLeft: 10, marginTop: 20 }}
                  onPress={() => {}}
                />
              </View>
              <View style={{ marginLeft: 20 }}>
                <Text style={{ marginTop: 20, color: "#00BFFF", fontSize: 17 }}>
                  Data de Envio:26/02/2020{" "}
                </Text>
                <Text style={{ marginTop: 20, color: "#C0C0C0", fontSize: 12 }}>
                  Aguardando
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {}}
              style={{
                alignSelf: "center",
                marginRight: 20,
                height: 20,
                width: 20
              }}
            >
              <IonIcons2
                name="chevron-right"
                color="#00BFFF"
                size={20}
                style={{}}
                onPress={() => {}}
              />
            </TouchableOpacity>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {}} style={styles.conteudo}>
            <View style={{ flexDirection: "row" }}>
              <View style={{}}>
                <IonIcons2
                  name="clipboard-list"
                  color="#00BFFF"
                  size={40}
                  style={{ marginLeft: 10, marginTop: 20 }}
                  onPress={() => {}}
                />
              </View>
              <View style={{ marginLeft: 20 }}>
                <Text style={{ marginTop: 20, color: "#00BFFF", fontSize: 17 }}>
                  Data de Envio:26/02/2020{" "}
                </Text>
                <Text style={{ marginTop: 20, color: "#C0C0C0", fontSize: 12 }}>
                  Aguardando
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {}}
              style={{
                alignSelf: "center",
                marginRight: 20,
                height: 20,
                width: 20
              }}
            >
              <IonIcons2
                name="chevron-right"
                color="#00BFFF"
                size={20}
                style={{}}
                onPress={() => {}}
              />
            </TouchableOpacity>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => {}} style={styles.conteudo}>
            <View style={{ flexDirection: "row" }}>
              <View style={{}}>
                <IonIcons2
                  name="clipboard-list"
                  color="#00BFFF"
                  size={40}
                  style={{ marginLeft: 10, marginTop: 20 }}
                  onPress={() => {}}
                />
              </View>
              <View style={{ marginLeft: 20 }}>
                <Text style={{ marginTop: 20, color: "#00BFFF", fontSize: 17 }}>
                  Data de Envio:26/02/2020{" "}
                </Text>
                <Text style={{ marginTop: 20, color: "#C0C0C0", fontSize: 12 }}>
                  Aguardando
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {}}
              style={{
                alignSelf: "center",
                marginRight: 20,
                height: 20,
                width: 20
              }}
            >
              <IonIcons2
                name="chevron-right"
                color="#00BFFF"
                size={20}
                style={{}}
                onPress={() => {}}
              />
            </TouchableOpacity>
          </TouchableOpacity>
        </ScrollView>
        <Footer />
      </View>
    );
  }
}
