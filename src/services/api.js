import axios from 'axios';

const api = axios.create({
    baseURL: 'http://apiremediando-com-br.umbler.net',
  });
  
  export default api;